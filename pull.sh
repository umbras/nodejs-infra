#!/bin/bash

rm -rf bundle/argo-cd
helm template app source -f source/values.yaml --output-dir bundle/. --namespace app --debug